#!/usr/bin/env python
# coding: utf-8

# In[20]:

import sys
import os
import io
import cv2
import math
from pdf2image import convert_from_path
from google.cloud import vision
from google.cloud.vision import types
from PIL import Image
import xlwt 
from xlwt import Workbook


# In[21]:


def convertToPDF(pdfpath, pdfname):
    pdf_file_name = pdfpath+pdfname
    filenameList = []
    page_list = convert_from_path(pdf_file_name, 500) 
    image_counter = 1
    for page in page_list: 
        imgname = str(image_counter)+".png"
        filenameList.append(str(image_counter))
        if(os.path.isdir(pdfpath+pdfname.rstrip('.pdf')) == False):
            os.mkdir(pdfpath+pdfname.rstrip('.pdf'))
        page.save(pdfpath+pdfname.rstrip('.pdf')+"/"+imgname, 'PNG')
#         print(image_counter)
        image_counter = image_counter + 1
    return filenameList

def resize_images(pdfpath, pdfname):
    width_list = []
    height_list = []
    folder_name = pdfpath+pdfname.rstrip('.pdf')+'/'
    for i in filename_list:
        filename = folder_name+str(i)+'.png'
        img = Image.open(filename)
        width, height = img.size
#         print(i, width, height)

        rwidth = 1000

        if width > rwidth:
            t_ratio = width/rwidth
            rheight = round(height/t_ratio)
            img_tmp = img.resize((rwidth, rheight), Image.ANTIALIAS)
            filename = folder_name+str(i)+'.png'
            img_tmp.save(filename)
#             print("resized ", rwidth, rheight)
            width = rwidth
            height = rheight
        width_list.append(width)
        height_list.append(height)
    return width_list, height_list 
            


# In[22]:


def detect_text(path):
    """Detects text in the file."""
    client = vision.ImageAnnotatorClient()
    with io.open(path, 'rb') as image_file:
        content = image_file.read()
    image = vision.types.Image(content=content)
    response = client.text_detection(image=image)
    return response


# In[23]:


def getLineCoord(px1, py1, px2, py2, px3, py3, px4, py4, lineList):
    x1 = []
    y1 = []
    x2 = []
    y2 = []
    x3 = []
    y3 = []
    x4 = []
    y4 = []
    
    jsonLine = {}
    num = len(lineList)
    line_list = []
#     print("length :", num)
    if len(lineList) == 1:
        coord = {}
        lin = {}
        s1 = px1
        if px4 < px1:
            s1 = px4
        s2 = px2
        if px3 > px2:
            s2 = px3
        t1 = py1
        if py2 < py1:
            t1 = py2
        t2 = py3
        if py4 > py3:
            t2 = py4
        
        coord['x1'] = s1
        coord['y1'] = t1
        coord['x3'] = s2
        coord['y3'] = t2
        lin['text'] = lineList[0]
        lin['coordinates'] = coord
        line_list.append(lin)
    else:
        s1 = px1
        if px4 < px1:
            s1 = px4
        s2 = px2
        if px3 > px2:
            s2 = px3
            
        t1 = py1
        if py2 < py1:
            t1 = py2
        t2 = py3
        if py4 > py3:
            t2 = py4
        
        
        u1 = t1
        ofy = math.ceil((t2-t1)/num)
        u2 = t1 
        for k in range(num):
            u2 = u2 + ofy
            if u2 > t2:
                u2 = t2
            coord = {}
            lin = {}
            coord['x1'] = s1
            coord['y1'] = u1
            coord['x3'] = s2
            coord['y3'] = u2
            lin['text'] = lineList[k]
            lin['coordinates'] = coord
            line_list.append(lin)
#             jsonLine['line-'+str(k)] = lin
            u1 = u2 + 1
    return line_list
    
def getBlocks(response):
#     jsonResponse = {}
    json_response = []
    document = response.full_text_annotation
    breaks = vision.enums.TextAnnotation.DetectedBreak.BreakType
    lnum = 0
    p = 0
    para = {}
    for page in document.pages:
        for block in page.blocks:
            for paragraph in block.paragraphs:
                
                px1 = paragraph.bounding_box.vertices[0].x
                py1 = paragraph.bounding_box.vertices[0].y
                px2 = paragraph.bounding_box.vertices[1].x
                py2 = paragraph.bounding_box.vertices[1].y
                px3 = paragraph.bounding_box.vertices[2].x
                py3 = paragraph.bounding_box.vertices[2].y
                px4 = paragraph.bounding_box.vertices[3].x
                py4 = paragraph.bounding_box.vertices[3].y
                
                paratext = ""
                line = ""
                lineList = []
                for word in paragraph.words:
                    for symbol in word.symbols:
                        line += symbol.text
                        if symbol.property.detected_break.type == breaks.SPACE:
                            line += ' '
                        if symbol.property.detected_break.type == breaks.EOL_SURE_SPACE:
                            line += ' '
                            paratext += line
                            lineList.append(line)
                            line = ''
                        elif symbol.property.detected_break.type == breaks.LINE_BREAK:
                            paratext += line
                            lineList.append(line)
                            line = ''
                jsonLine = getLineCoord(px1, py1, px2, py2, px3, py3, px4, py4, lineList)
                par = {}
                text = {}
                coord = {}

                s1 = px1
                if px4 < px1:
                    s1 = px4
                s2 = px2
                if px3 > px2:
                    s2 = px3

                t1 = py1
                if py2 < py1:
                    t1 = py2
                t2 = py3
                if py4 > py3:
                    t2 = py4
                
                coord['x1'] = s1
                coord['y1'] = t1
                coord['x3'] = s2
                coord['y3'] = t2
                par['text'] = paratext
                par['coordinates'] = coord
                par['line'] = jsonLine
                para['para-'+str(p)] = par
                
                json_tmp = {}
#                 json_tmp['text'] = paratext.strip()
                json_tmp['px1'] = s1
                json_tmp['py1'] = t1
                json_tmp['px2'] = s2
                json_tmp['py2'] = t2
                
                json_tmp['line']  = jsonLine
                json_tmp['para_number'] = p
                json_response.append(json_tmp)
                p = p + 1
#         jsonResponse['response'] = para
#     return jsonResponse
    return json_response


# In[24]:


def drawParagraph(img, jsonResponse):
    noofpara = len(jsonResponse['response'])
    for k in range(noofpara):
        x1 = jsonResponse['response']['para-'+str(k)]['coordinates']['x1']
        y1 = jsonResponse['response']['para-'+str(k)]['coordinates']['y1']
        x3 = jsonResponse['response']['para-'+str(k)]['coordinates']['x3']
        y3 = jsonResponse['response']['para-'+str(k)]['coordinates']['y3']
        cv2.rectangle(img,(x1, y1),(x3, y3),(34,48,245),3)
    return img
def drawLine(img, jsonResponse):
    noofpara = len(jsonResponse['response'])
    for k in range(noofpara):
        nooflines = len(jsonResponse['response']['para-'+str(k)]['line'])
        for l in range(nooflines):
            x1 = jsonResponse['response']['para-'+str(k)]['line']['line-'+str(l)]['coordinates']['x1']
            y1 = jsonResponse['response']['para-'+str(k)]['line']['line-'+str(l)]['coordinates']['y1']
            x3 = jsonResponse['response']['para-'+str(k)]['line']['line-'+str(l)]['coordinates']['x3']
            y3 = jsonResponse['response']['para-'+str(k)]['line']['line-'+str(l)]['coordinates']['y3']
            cv2.rectangle(img,(x1, y1),(x3, y3),(127,62, 20),3)
    return img


# In[25]:


def writeXLS(wb, sno, jsonResponse):
    if wb == 0:
        wb = Workbook()
    sheet = wb.add_sheet('Sheet '+sno, cell_overwrite_ok=True)
    noofpara = len(jsonResponse['response'])
    row = 1
    sheet.write(0, 0, 'Paragraph')
    sheet.write(0, 1, 'Text')
    sheet.write(0, 2, 'x1')
    sheet.write(0, 3, 'y1')
    sheet.write(0, 4, 'x2')
    sheet.write(0, 5, 'y2')
    for k in range(noofpara):
        nooflines = len(jsonResponse['response']['para-'+str(k)]['line'])
        for l in range(nooflines):
            x1 = jsonResponse['response']['para-'+str(k)]['line']['line-'+str(l)]['coordinates']['x1']
            y1 = jsonResponse['response']['para-'+str(k)]['line']['line-'+str(l)]['coordinates']['y1']
            x3 = jsonResponse['response']['para-'+str(k)]['line']['line-'+str(l)]['coordinates']['x3']
            y3 = jsonResponse['response']['para-'+str(k)]['line']['line-'+str(l)]['coordinates']['y3']
            
            text = jsonResponse['response']['para-'+str(k)]['line']['line-'+str(l)]['text']

            sheet.write(row, 0, 'para'+str(k+1))
            sheet.write(row, 1, text)
            sheet.write(row, 2, x1)
            sheet.write(row, 3, y1)
            sheet.write(row, 4, x3)
            sheet.write(row, 5, y3)
            row += 1
    return wb
def saveXLS(wb, outxls):
    wb.save(outxls)


# In[26]:


def get_response_list(pdfpath, pdfname, filename_list):
    response_list = []
    for i in range(len(filename_list)):
        response_block = {}
        uri = pdfpath+pdfname.rstrip('.pdf')+"/"+filename_list[i]+'.png'
        response_block['image_name'] = uri
        response_block['image_width'] = width_list[i]
        response_block['image_height'] = height_list[i]
        try:
            imgnumber = i
            response = detect_text(uri)
            response_block['block'] = getBlocks(response)
            response_list.append(response_block)
        except Exception as e:
            response_list.append(response_block)
    return response_list


# In[27]:


def format_data(response_list):
    format_response = []
    for rl in response_list:
        page_info = {}
        page_info['image_name'] = rl['image_name']
        page_info['image_width'] = rl['image_width']
        page_info['image_height'] = rl['image_height']


        block_list = []
    #     print(rl)

        for bl in rl['block']:
            for li in bl['line']:
                block_info = {}
                block_info['text'] = li['text']
                block_info['x1'] = li['coordinates']['x1']
                block_info['y1'] = li['coordinates']['y1']
                block_info['x2'] = li['coordinates']['x3']
                block_info['y2'] = li['coordinates']['y3']
                block_list.append(block_info)

        page_info['block'] = block_list

        format_response.append(page_info)
    return format_response
# print(format_response)
# for i in range(len(response_list[0]['block'])):
    
#     print(response_list[0]['block'][i]['line'])


# In[28]:


# pdfpath = 'pdfs/'

# # pdfname = '18 hours keong saik menu-20190319142437.pdf'
# pdfname = 'chang ji herbal duck menu-20190402160901.pdf'


# filename_list = convertToPDF(pdfpath, pdfname)
# width_list, height_list = resize_images(pdfpath, pdfname)
# response_list = get_response_list(pdfpath, pdfname, filename_list)
# format_response = format_data(response_list)
# print(format_response)

if __name__ == "__main__":
    pdfpath = sys.argv[1]
    pdfname = sys.argv[2]
    filename_list = convertToPDF(pdfpath, pdfname)
    width_list, height_list = resize_images(pdfpath, pdfname)
    response_list = get_response_list(pdfpath, pdfname, filename_list)
    format_response = format_data(response_list)
    print(format_response)

#     print(jsonKey)
    sys.stdout.flush()


# In[ ]:




